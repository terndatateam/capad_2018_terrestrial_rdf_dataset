import fiona
import rtree
from shapely.geometry import shape, Point

from rdflib import Graph
from rdflib.namespace import RDF, SKOS

import time
import logging


def _fetch(URL):
    logging.info(f'Fetching shapefile from swift store: {URL}')
    start_time = time.time()
    shp = fiona.open(URL)
    logging.info(f'Finished fetching in {(time.time() - start_time):.2f} seconds')
    return shp


def get_shp(URL):
    return _fetch(URL)


def _index_features(shp):
    logging.info(f'Creating index')
    start_time = time.time()
    index = rtree.index.Index()

    for i, feature in enumerate(shp):
        geometry = shape(feature['geometry'])
        index.insert(i, geometry.bounds)

    logging.info(f'Finished creating index in {(time.time() - start_time):.2f} seconds')
    return index


def get_index(shp):
    logging.info(f'Getting index')
    index = _index_features(shp)
    return index


def get_feature(latitude, longitude, url):
    point = Point(longitude, latitude)
    shp = get_shp(url)
    for feature in shp:
        print(feature['properties'])
    index = get_index(shp)

    start_time = time.time()

    ids = [int(i) for i in index.intersection(point.coords[0])]
    for id in ids:
        if point.within(shape(shp[id]['geometry'])):
            logging.info(f'Point within found in {(time.time() - start_time):.2f} seconds')
            return shp[id]['properties']


def get_rdf_graph(URL, format='turtle'):
    logging.info(f'Getting RDF from {URL}')
    start_time = time.time()
    g = Graph().parse(URL, format=format)
    logging.info(f'RDF retrieved in {time.time() - start_time:.2f}')
    return g


def get_rdf_uri(URL, predicate_value, object_value):
    g = get_rdf_graph(URL)

    for concept, _, _ in g.triples((None, RDF.type, SKOS.Concept)):
        for _, _, value in g.triples((concept, predicate_value, None)):
            if str(value) == str(object_value):
                return str(concept)