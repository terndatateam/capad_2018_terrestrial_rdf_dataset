from rdflib import Graph
import requests

import logging
import time


if __name__ == '__main__':
    start_time = time.time()
    logging.basicConfig(level=logging.INFO)

    g = Graph().parse('output.ttl', format='turtle')

    URL = 'https://graphdb-850.tern.org.au/repositories/capad_2018_terrestrial/statements'
    r = requests.post(URL, data=g.serialize(format='turtle'), auth=(USERNAME, PASSWORD), headers={'content-type': 'text/turtle'})
    logging.info(f'Status code: {r.status_code}')
    logging.info(f'Message: {r.content.decode("utf-8")}')

    logging.info(f'Completed in {(time.time() - start_time):.2f} seconds')