FROM osgeo/gdal:alpine-small-3.0.4

RUN apk update
RUN apk add --no-cache python3 python3-dev build-base make wget cmake libc-dev geos-dev

# Download libspatialindex
WORKDIR /home/spatial/
RUN wget https://github.com/libspatialindex/libspatialindex/releases/download/1.9.3/spatialindex-src-1.9.3.tar.gz
RUN tar -xvzf spatialindex-src-1.9.3.tar.gz

# Install libspatialindex
WORKDIR /home/spatial/spatialindex-src-1.9.3/
RUN cmake .
RUN make
RUN make install

RUN pip3 install --no-cache-dir --upgrade pip \
    && pip3 install --no-cache-dir --upgrade setuptools

WORKDIR /home/
COPY requirements.txt .
RUN pip3 install --no-cache-dir -r requirements.txt

WORKDIR /home/app/
RUN chown -R 1001:1001 /home/app
USER 1001

CMD python3 app.py