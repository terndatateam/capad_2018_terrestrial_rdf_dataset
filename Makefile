# Build image.
build:
	docker build -t ternau/capad_2018_terrestrial_rdf_dataset .

run:
	docker run --rm -it -v ${PWD}:/home/app ternau/capad_2018_terrestrial_rdf_dataset

# Push to Docker Hub.
push:
	docker push ternau/capad_2018_terrestrial_rdf_dataset:latest