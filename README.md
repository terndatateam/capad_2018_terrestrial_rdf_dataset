# Collaborative Australian Protected Areas Database (CAPAD) 2018 - Terrestrial

This Python program reads in the shapefiles of the CAPAD 2018 - Terrestrial and converts it to an RDF dataset in GeoSPARQL. 